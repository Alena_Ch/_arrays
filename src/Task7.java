import java.util.Random;

public class Task7 {
    public static void main(String[] args) {
        int[][] arr = new int[7][];
        Random rand = new Random();
        System.out.println("Наш многомерный массив имеет вид: ");

        for (int i = 0; i < arr.length; i++) {
            arr[i] = new int[rand.nextInt(8) + 1];
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = rand.nextInt(8) + 1;
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("\n");
        }
        //ищем среди строк ММ самый короткий ОМ
        int[] numMin = arr[0];


        for (int i = 0; i < arr.length; i++) {
            if (arr[i].length < numMin.length) {
                numMin = arr[i];

            }

        }
        System.out.println("Если у нас есть несколько ОМ с наименьшей длиной, берем первый");

        //Выводим самый короткий одномерный массив
        System.out.println("Самый короткий одномерный массив: ");
        for (int f = 0; f < numMin.length; f++) {
            System.out.print(numMin[f] + " ");

        }
        System.out.println("\n");

        //Подсчитываем сумму элементов самого короткого одномерного массива
        int sumNumMin = 0;
        for (int n = 0; n < numMin.length; n++) {

            sumNumMin = sumNumMin + numMin[n];
        }
        System.out.print("Сумма элементов самого короткого одномерного массива = " + sumNumMin);
    }
}
