import java.util.Random;


public class Task8 {
    public static void main(String[] args) {

        int[][] arr = new int[8][];
        Random rand = new Random();
        System.out.println("Наш многомерный массив имеет вид: ");

        for (int i = 0; i < arr.length; i++) {
            arr[i] = new int[rand.nextInt(9) + 1];
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = rand.nextInt(9) + 1;
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("\n");
        }
        System.out.println("Выводим суммы элементов ОМ с четными индексами");

        for (int i = 0; i < arr.length; i++) {
            int sumEvenInd = 0;
            if (i % 2 == 1) {
                for (int j = 0; j < arr[i].length; j++) {
                    sumEvenInd = sumEvenInd + arr[i][j];
                }
                System.out.println(sumEvenInd + " ");
            }
        }
    }
}

