public class Task5 {
    public static void main(String[] args) {
        int[] arr1 = new int[5];
        int[] arr2 = new int[5];
        double sum1 = 0;
        double sum2 = 0;
        double a1;
        double a2;

        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = (int) (Math.random() * 6);
            System.out.print(arr1[i] + " ");
        }
        System.out.println("\n");

        for (int i = 0; i < arr2.length; i++) {
            arr2[i] = (int) (Math.random() * 6);
            System.out.print(arr2[i] + " ");
        }
        System.out.println("\n");

        for (int i = 0; i < arr1.length; i++) {
            sum1 += arr1[i];
        }

        a1 = sum1 / 5;
        System.out.println(a1);

        System.out.println("\n");
        for (int i = 0; i < arr2.length; i++) {
            sum2 += arr2[i];
        }

        a2 = sum2 / 5;
        System.out.println(a2);

        if (a1 > a2) {
            System.out.println("Среднее арифмитическое массива arr1 больше чем среднее арифмитическое массива arr2");
        } else if (a2 > a1) {
            System.out.println("Среднее арифмитическое массива arr2 больше чем среднее арифмитическое массива arr1");
        } else if (a1 == a2) {
            System.out.println("Среднее арифмитическое массивов arr1 и arr2 равны");
        }
    }
}
