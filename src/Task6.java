import java.util.Random;

public class Task6 {
    public static void main(String[] args) {
        int[][] arr = new int[5][];
        Random rand = new Random();
        System.out.println("Наш многомерный массив имеет вид: ");

        for (int i = 0; i < arr.length; i++) {
            arr[i] = new int[rand.nextInt(10) + 1];
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = rand.nextInt(10) + 1;
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("\n");
        }
        //ищем среди строк ММ самый длинный ОМ
        int[] numMAx = arr[0];
        for (int i = 0; i < arr.length; i++) {

            if (arr[i].length > numMAx.length) {
                numMAx = arr[i];
            }
        }
        System.out.println("Если у нас есть несколько ОМ с наибольшей длиной, берем первый");
        //Выводим самый длинный одномерный массив
        System.out.println("Самый длинный одномерный массив: ");
        for (int i = 0; i < numMAx.length; i++) {
            System.out.print(numMAx[i] + " ");

        }
        System.out.println("\n");

        //Подсчитываем сумму элементов самого длинного одномерного массива
        int sumNumMax = 0;
        for (int k = 0; k < numMAx.length; k++) {

            sumNumMax = sumNumMax + numMAx[k];
        }
        System.out.print("Сумма элементов самого длинного одномерного массива = " + sumNumMax);
    }
}














