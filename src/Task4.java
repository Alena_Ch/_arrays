public class Task4 {
    public static void main(String[] args) {
        int[] arr = new int[8];
        int n = 0;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 10 + 1);
            System.out.print(arr[i] + " ");
        }
        System.out.println("\n");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 1) {
                arr[i] = n;
            }
            System.out.print(arr[i] + " ");
        }
    }
}
